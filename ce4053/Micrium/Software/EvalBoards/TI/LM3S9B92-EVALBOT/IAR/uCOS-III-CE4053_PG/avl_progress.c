#include<stdio.h>
#include<stdlib.h>
#include<os.h>

typedef struct node
{
	OS_TICK data;
	OS_TCB *Rdy_tcb;
	struct node *left,*right;
	int ht;
}node;



node *insert(node *,OS_TCB*); //Pass Tree and Task's TCB
node *Delete(node *,int);
//void preorder(node *);
//void inorder(node *);
node *minimum(node*);
int height( node *);
node *rotateright(node *);
node *rotateleft(node *);
node *RR(node *);
node *LL(node *);
node *LR(node *);
node *RL(node *);
int BF(node *);


OS_MEM *AVL_Partition;


node * insert(node *T,OS_TCB *p_tcb)
{
	OS_TICK x=0;
	x= p_tcb->Deadline; //x holds the task's deadline
	
	if(T==NULL)
	{
		T=(node*)malloc(sizeof(node));
		T->Rdy_tcb=p_tcb;
		T->data=p_tcb->Deadline;
		T->left=NULL;
		T->right=NULL;
	}
	else
		if(x > T->data)		// if new task's deadline is greater than root's deadline then insert in right subtree
		{
			T->right=insert(T->right,p_tcb);
			if(BF(T)==-2)
				if(x>T->right->data)
					T=RR(T);
				else
					T=RL(T);
		}
		else
			if(x<T->data)
			{
				T->left=insert(T->left,p_tcb);
				if(BF(T)==2)
					if(x < T->left->data)
						T=LL(T);
					else
						T=LR(T);
			}
		
		T->ht=height(T);
		
		return(T);
}

node * Delete(node *T,int x)
{
	node *p;
	
	if(T==NULL)
	{
		return NULL;
	}
	else
		if(x > T->data)		// insert in right subtree
		{
			T->right=Delete(T->right,x);
			if(BF(T)==2)
				if(BF(T->left)>=0)
					T=LL(T);
				else
					T=LR(T);
		}
		else
			if(x<T->data)
			{
				T->left=Delete(T->left,x);
				if(BF(T)==-2)	//Rebalance during windup
					if(BF(T->right)<=0)
						T=RR(T);
					else
						T=RL(T);
			}
			else
			{
				//data to be deleted is found
				if(T->right!=NULL)
				{	//delete its inorder succesor
					p=T->right;
					
					while(p->left!= NULL)
						p=p->left;
					
					T->data=p->data;
					T->right=Delete(T->right,p->data);
					
					if(BF(T)==2)//Rebalance during windup
						if(BF(T->left)>=0)
							T=LL(T);
						else
							T=LR(T);\
				}
				else
					return(T->left);
			}
	T->ht=height(T);
	return(T);
}

int height(node *T)
{
	int lh,rh;
	if(T==NULL)
		return(0);
	
	if(T->left==NULL)
		lh=0;
	else
		lh=1+T->left->ht;
		
	if(T->right==NULL)
		rh=0;
	else
		rh=1+T->right->ht;
	
	if(lh>rh)
		return(lh);
	
	return(rh);
}


node * rotateright(node *x)
{
	node *y;
	y=x->left;
	x->left=y->right;
	y->right=x;
	x->ht=height(x);
	y->ht=height(y);
	return(y);
}

node * rotateleft(node *x)
{
	node *y;
	y=x->right;
	x->right=y->left;
	y->left=x;
	x->ht=height(x);
	y->ht=height(y);
	
	return(y);
}


node * RR(node *T)
{
	T=rotateleft(T);
	return(T);
}

node * LL(node *T)
{
	T=rotateright(T);
	return(T);
}

node * LR(node *T)
{
	T->left=rotateleft(T->left);
	T=rotateright(T);
	
	return(T);
}

node * RL(node *T)
{
	T->right=rotateright(T->right);
	T=rotateleft(T);
	return(T);
}

int BF(node *T)
{
	int lh,rh;
	if(T==NULL)
		return(0);

	if(T->left==NULL)
		lh=0;
	else
		lh=1+T->left->ht;

	if(T->right==NULL)
		rh=0;
	else
		rh=1+T->right->ht;

	return(lh-rh);
}

node* minimum(node *T)
{
	node* tempT;
	tempT = T;
	if(T==NULL)
		printf("Empty");
	while(T!=NULL && T->left!=NULL)
		T=T->left;
	printf("\nMinimum is %d", T->data);
	T= Delete(tempT, T->data);
        return(T);
	
}

