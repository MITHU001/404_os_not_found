#include<stdio.h>
#include<stdlib.h>
#include<os.h>

typedef struct AVLNode
{
	OS_TICK data;
	OS_TCB *Rdy_tcb;
	struct AVLNode *left,*right;
	int ht;
}AVLNode;



AVLNode* insert(AVLNode *,OS_TCB*); //Pass Tree and Task's TCB
AVLNode *Delete(AVLNode *,int);
//void preorder(AVLNode *);
//void inorder(AVLNode *);
AVLNode *minimum(AVLNode*);
int height( AVLNode *);
AVLNode *rotateright(AVLNode *);
AVLNode *rotateleft(AVLNode *);
AVLNode *RR(AVLNode *);
AVLNode *LL(AVLNode *);
AVLNode *LR(AVLNode *);
AVLNode *RL(AVLNode *);
int BF(AVLNode *);

extern AVLNode *AVLTree;

OS_MEM AVLStoragePtr;
    
CPU_INT32U AVLStorage[10][20];

OS_ERR AVLStorageCreate()
{
  //AVLStoragePtr= (OS_MEM*)malloc(sizeof(OS_MEM));
    
  OS_ERR err;
  OSMemCreate((OS_MEM*) &AVLStoragePtr,
                  (CPU_CHAR*) "AVLStorageRegion",
                  &AVLStorage[0][0],
                  (OS_MEM_QTY) 10,
                  (OS_MEM_SIZE) sizeof(CPU_INT32U)*20,
                  (OS_ERR*) &err
                    );
  return err;

}
AVLNode* insert(AVLNode *T,OS_TCB *p_tcb)
{
	OS_ERR err;
        OS_TICK x=0;
	x= p_tcb->Period; //x holds the task's deadline
	
	if(T==NULL)
	{
		T=(AVLNode*)OSMemGet((OS_MEM*)&AVLStoragePtr,
                                   (OS_ERR*)&err);
                if(err==OS_ERR_NONE)
                {
                  T->Rdy_tcb=(OS_TCB*)p_tcb;
                  T->data=p_tcb->Period;
                  T->left=NULL;
                  T->right=NULL;
                }
	}
	else
		if(x > T->data)		// if new task's deadline is greater than root's deadline then insert in right subtree
		{
			T->right=insert(T->right,p_tcb);
			if(BF(T)==-2)
				if(x>T->right->data)
					T=RR(T);
				else
					T=RL(T);
		}
		else
			if(x<T->data)
			{
				T->left=insert(T->left,p_tcb);
				if(BF(T)==2)
					if(x < T->left->data)
						T=LL(T);
					else
						T=LR(T);
			}
		
		T->ht=height(T);
		
		return(T);
}

AVLNode * Delete(AVLNode *T,int x)
{
	AVLNode *p;
	
	if(T==NULL)
	{
		return NULL;
	}
	else
		if(x > T->data)		// insert in right subtree
		{
			T->right=Delete(T->right,x);
			if(BF(T)==2)
				if(BF(T->left)>=0)
					T=LL(T);
				else
					T=LR(T);
		}
		else
			if(x<T->data)
			{
				T->left=Delete(T->left,x);
				if(BF(T)==-2)	//Rebalance during windup
					if(BF(T->right)<=0)
						T=RR(T);
					else
						T=RL(T);
			}
			else
			{
				//data to be deleted is found
				if(T->right!=NULL)
				{	//delete its inorder succesor
					p=T->right;
					
					while(p->left!= NULL)
						p=p->left;
					
					T->data=p->data;
                                        T->Rdy_tcb=p->Rdy_tcb;
					T->right=Delete(T->right,p->data);
					
					if(BF(T)==2)//Rebalance during windup
						if(BF(T->left)>=0)
							T=LL(T);
						else
							T=LR(T);\
				}
				else
					return(T->left);
			}
	T->ht=height(T);
	return(T);
}

int height(AVLNode *T)
{
	int lh,rh;
	if(T==NULL)
		return(0);
	
	if(T->left==NULL)
		lh=0;
	else
		lh=1+T->left->ht;
		
	if(T->right==NULL)
		rh=0;
	else
		rh=1+T->right->ht;
	
	if(lh>rh)
		return(lh);
	
	return(rh);
}


AVLNode * rotateright(AVLNode *x)
{
	AVLNode *y;
	
        y=x->left;
       // if(y!=NULL)
         // y->Rdy_tcb=x->left->Rdy_tcb;
	
        x->left=y->right;
        //if(x->left!=NULL)
          //x->left->Rdy_tcb=y->right->Rdy_tcb;
	
        y->right=x;
        //if(y->right!=NULL)
          //y->right->Rdy_tcb=x->Rdy_tcb;
	
        x->ht=height(x);
	y->ht=height(y);
	return(y);
}

AVLNode * rotateleft(AVLNode *x)
{
	AVLNode *y;
	
        y=x->right;
        //if(y!=NULL)
         // y->Rdy_tcb=x->right->Rdy_tcb;
	
        x->right=y->left;
        //if(x->right!=NULL)
          //x->right->Rdy_tcb=y->left->Rdy_tcb;
	
        y->left=x;
        //if(y->left!=NULL)
          //y->left->Rdy_tcb=x->Rdy_tcb;
	
        x->ht=height(x);
	y->ht=height(y);
	
	return(y);
}


AVLNode * RR(AVLNode *T)
{
	T=rotateleft(T);
	return(T);
}

AVLNode * LL(AVLNode *T)
{
	T=rotateright(T);
	return(T);
}

AVLNode * LR(AVLNode *T)
{
	T->left=rotateleft(T->left);
	T=rotateright(T);
	
	return(T);
}

AVLNode * RL(AVLNode *T)
{
	T->right=rotateright(T->right);
	T=rotateleft(T);
	return(T);
}

int BF(AVLNode *T)
{
	int lh,rh;
	if(T==NULL)
		return(0);

	if(T->left==NULL)
		lh=0;
	else
		lh=1+T->left->ht;

	if(T->right==NULL)
		rh=0;
	else
		rh=1+T->right->ht;

	return(lh-rh);
}

AVLNode* minimum(AVLNode *T)
{
	AVLNode* tempT;
        AVLNode* min;
	tempT = T;
	if(T==NULL )
        {
         // AVLTree = Delete(AVLTree, T->data);
         // AVLTree = NULL;
          return (T);
        }
       
	while(T!=NULL && T->left!=NULL)
		T=T->left;
	
	// AVLTree = T;
        // BEcause value in address gets swapped!
        //AVLTree = Delete(AVLTree, T->data);
       // AVLTree = T;
        return(T);
	
}

